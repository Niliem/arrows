﻿using UnityEngine;
using UnityEngine.UI;

public class ArrowDisplay : PoolObject 
{
	public Sprite ArrowSprite;

	private ArrowDirection _direction; 
	private Image _image;

	private void Awake() 
	{
		_image = GetComponent<Image>();
	}

	public void SetDirection(ArrowDirection direction)
	{
		_direction = direction;
		_image.transform.rotation = Quaternion.Euler( Vector3.forward * (int)_direction * 90.0f);
	}

}
