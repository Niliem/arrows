﻿using System.Collections.Generic;
using UnityEngine;
using Audio;

[System.Serializable]
public enum ArrowDirection
{
	Up
	, Left
	, Down
	, Right
};
	
[System.Serializable]
public class Line
{
	private List<ArrowDirection> _lineDirections;
	private int _typeIndex;
	private LineDisplay _lineDisplay;

	public Line(List<ArrowDirection> line, LineDisplay lineDisplay)
	{
		_lineDirections = line;
		_typeIndex = 0;
		_lineDisplay = lineDisplay;
		_lineDisplay.SetLine (line);
	}

	public ArrowDirection GetNextDirection()
	{
		return _lineDirections [_typeIndex];
	}

	public void TypeDirection()
	{
		_typeIndex++;
		_lineDisplay.RemoveDirection ();
		AudioManager.Instance.Play("Arrows");
	}

	public bool Typed()
	{
		bool typed = (_typeIndex >= _lineDirections.Count);
		if (typed) 
		{
			_lineDisplay.RemoveLine (); 
		}
		return typed;
	}

	public void TypeLine()
	{
		_lineDisplay.RemoveLine();
	}

	public void SetLocation(float x, float y)
	{
		_lineDisplay.transform.position = new Vector2(x, y);
	}

	public void SetSpeed(float speed)
	{
		_lineDisplay.Speed = speed;
	}

	public float width
	{
		get { return _lineDisplay.Width; }
	}

	public void SetCurrent(bool current)
	{
		_lineDisplay.SetCurrent(current);
	}
	
}
