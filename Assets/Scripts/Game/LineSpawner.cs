﻿using UnityEngine;

public class LineSpawner : MonoBehaviour
{
	public GameObject LinePref;
	public Transform ParentTransform;

	private void Start() 
	{
		PoolManager.Instance.CreatePool(LinePref, 1);
	}

	public LineDisplay SpawnLine()
	{
		GameObject go =  PoolManager.Instance.Get(LinePref);
		//GameObject go = Instantiate(LinePref) as GameObject;
		go.transform.SetParent (ParentTransform);
		go.transform.localScale = Vector3.one;
		return go.GetComponent<LineDisplay>();
	}
}
