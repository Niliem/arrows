﻿using UnityEngine;

public class LineTimer : MonoBehaviour
{
	public TimerSettings Settings;

	private float _currentTime;
	private float _currentSpawnDelay;
	private float _currentSpeed;

	private void Start() 
	{
		Reset();
	}

	private void Update () 
	{
		_currentTime -= Time.deltaTime;
		if(_currentTime <= 0.0f)
		{
			LineManager.Instance.AddLine(_currentSpeed);
			if(_currentSpeed <= Settings.MaxSpeed)
				_currentSpeed *= Settings.SpeedInc;
			_currentTime = _currentSpawnDelay;
			if(_currentSpawnDelay >= Settings.MinSpawnDelay)
				_currentSpawnDelay *= Settings.SpawnDelayDec;
		}
	}

	public void Reset() 
	{
		_currentTime = Settings.SpawnDelay;
		_currentSpawnDelay = Settings.SpawnDelay;
		_currentSpeed = Settings.Speed;
	}

}
