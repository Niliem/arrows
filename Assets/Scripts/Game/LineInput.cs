﻿using UnityEngine;

public class LineInput : MonoBehaviour 
{
	// TODO: REMOVE
	void Update () 
	{
		if (Input.GetKeyDown (KeyCode.UpArrow)) 
		{
			TypeDirection(ArrowDirection.Up);
		}
		if (Input.GetKeyDown (KeyCode.DownArrow)) 
		{	
			TypeDirection(ArrowDirection.Down);
		}
		if (Input.GetKeyDown (KeyCode.LeftArrow)) 
		{
			TypeDirection(ArrowDirection.Left);
		}
		if (Input.GetKeyDown (KeyCode.RightArrow)) 
		{
			TypeDirection(ArrowDirection.Right);
		}

		if(Input.GetKeyDown(KeyCode.Space))
		{
			LineManager.Instance.TypeAll();
		}
	}

	public void Up()
	{
		TypeDirection(ArrowDirection.Up);
	}

	public void Down()
	{
		TypeDirection(ArrowDirection.Down);
	}

	public void Left()
	{
		TypeDirection(ArrowDirection.Left);
	}

	public void Right()
	{
		TypeDirection(ArrowDirection.Right);
	}

	public void TypeDirection(ArrowDirection direction)
	{
		LineManager.Instance.TypeDirection (direction);
	}
	
}
