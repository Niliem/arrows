﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LineDisplay : PoolObject 
{
	public GameObject ArrowPref;

	private float _speed = 1.0f;
	private Queue<GameObject> _sprites;
	private List<ArrowDirection> _line;
	private Image _borderImage;
	private RectTransform _rectTransform;
	private bool isEnd = false;

	private void Awake()
	{
		_sprites = new Queue<GameObject> ();
		_rectTransform = GetComponent<RectTransform>();
		_borderImage = GetComponent<Image>();
		_borderImage.enabled = false;
	}

	private void Start() 
	{
		PoolManager.Instance.CreatePool(ArrowPref, 3);
	}

	// TODO: REMOVE
	private void Update()
	{
		transform.Translate(Vector2.down * Time.deltaTime * _speed);
		if(transform.position.y < LineManager.Instance.MinY && !isEnd)
		{
			Time.timeScale = 0.0f;
			GameSceneManager.Instance.SetState(GameScene.Fail);
			isEnd = true;
		}
	}

	public void SetLine(List<ArrowDirection> line)
	{
		_line = line;
		CreateLine ();
	}

	public void RemoveDirection()
	{
		PoolManager.Instance.Back(_sprites.Dequeue ());
		//Destroy(_sprites.Dequeue ());
		_rectTransform.sizeDelta = new Vector2 (_sprites.Count * 130, 130);
	}

	public void RemoveLine()
	{
		while(_sprites.Count > 0)
		{
			PoolManager.Instance.Back(_sprites.Dequeue ());
			//Destroy(_sprites.Dequeue ());
		}
		PoolManager.Instance.Back(gameObject);
		//Destroy (gameObject);
	}

	private void CreateLine()
	{
		foreach (var direction in _line) 
		{
			GameObject sprite =  PoolManager.Instance.Get(ArrowPref);
			//GameObject sprite = Instantiate (ArrowPref);
			sprite.transform.SetParent (transform);
			sprite.transform.localScale = Vector3.one;
			sprite.GetComponent<ArrowDisplay> ().SetDirection (direction);
			_sprites.Enqueue (sprite);
		}
		_rectTransform.sizeDelta = new Vector2 (_sprites.Count * 130, 130);
	}

	public float Speed
	{
		set{ _speed = value; }
	}

	public float Width
	{
		get { return _rectTransform.sizeDelta.x; }
	}

	public void SetCurrent(bool current)
	{
		_borderImage.enabled = current;
	}

}
