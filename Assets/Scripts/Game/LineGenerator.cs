﻿using System.Collections.Generic;
using UnityEngine;

public class LineGenerator : MonoBehaviour
{
	public int MinLineLength = 3;
	public int MaxLineLength = 7;
	
	public List<ArrowDirection> GetRandomArrowLine()
	{
		List<ArrowDirection> line = new List<ArrowDirection> ();
		int length = Random.Range (MinLineLength, MaxLineLength);
		for (int i = 0; i < length; i++) 
		{
			ArrowDirection dir = (ArrowDirection)Random.Range (0, 4);
			line.Add(dir);
		}
		return line;
	}
}
