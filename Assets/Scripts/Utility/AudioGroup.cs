﻿using UnityEngine;

namespace Audio
{
	[System.Serializable]
	public class AudioGroup
	{
		public string name;
		public string paramName;
		public bool muted;

		[Range(-80.0f, 0.0f)]
		public float volume;
	}
}
