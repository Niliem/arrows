﻿using UnityEngine;

public class PoolObject : MonoBehaviour 
{
	private bool _isFree;
	private Transform _holder;

	public bool isFree
	{
		get { return _isFree; }
        set
        {
            _isFree = value;
            gameObject.SetActive(!value);
        }
	}

	public Transform Holder
	{
		get { return _holder; }
		set { _holder = value; }
	}
}
