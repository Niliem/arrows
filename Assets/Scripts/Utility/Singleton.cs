﻿using UnityEngine;

public class Singleton<T> : MonoBehaviour where T : Component 
{
	private static T _instance;

	public virtual void Awake ()
    {
		if (_instance == null) 
		{
			_instance = this as T;
		}
		else 
		{
			Destroy (gameObject);
		}
	}

	public static T Instance
	{
		get
		{
			if(_instance == null)
			{
				Debug.Log("[Singleton] An instance of " + typeof(T) + " is needed in the scene");
			}
			return _instance;
		}
	}

}
