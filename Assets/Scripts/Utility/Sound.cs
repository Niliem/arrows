﻿using UnityEngine;
using UnityEngine.Audio;

namespace Audio
{
	[System.Serializable]
	public class Sound 
	{
		public string name;
		public AudioClip audioClip;
		public AudioMixerGroup mixerGroup;

		[Range(0.0f, 1.0f)]
		public float volume;
		public bool loop;

		[HideInInspector]
		public AudioSource source;
	}
}
