﻿using UnityEngine;
using UnityEngine.UI;
using Audio;

public class ToggleAudioButton : MonoBehaviour 
{
	public string audioGroupName;
	public Sprite onSprite;
	public Sprite offSprite;
	private bool isMuted;
	private Image image;

	private void Awake() 
	{
		image = GetComponent<Image>();
	}
	private void Start() 
	{
		isMuted = AudioManager.Instance.isMuteAudioGroup(audioGroupName);
		UpdateSprite();
	}

	public void ToggleAudio()
	{
		isMuted = !isMuted;
		AudioManager.Instance.MuteAudioGroup(audioGroupName, isMuted);
		UpdateSprite();
	}

	private void UpdateSprite()
	{
		image.sprite = (isMuted) ? offSprite : onSprite;
	}
}
