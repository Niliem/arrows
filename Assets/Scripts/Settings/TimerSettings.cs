﻿[System.Serializable]
public class TimerSettings
{
	public float SpawnDelay = 3.0f;
	public float SpawnDelayDec = 0.97f;
	public float MinSpawnDelay = 1.5f;
	public float Speed = 1.0f;
	public float SpeedInc = 1.02f;
	public float MaxSpeed = 2.0f;
}
