﻿using UnityEngine;

public class MenuSceneManager : Singleton<MenuSceneManager> 
{
	public GameObject MainCanvas;

	private void Start() 
	{	
		GetComponent<Canvas>().worldCamera = Camera.main;
		MainCanvas.SetActive(true);
	}

	public void StartGame()
	{
		GameManager.Instance.StartGame();
	}
	
}
