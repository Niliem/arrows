﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PoolManager : Singleton<PoolManager> 
{
	private Dictionary<int, List<PoolObject>> _poolDictionary = new Dictionary<int, List<PoolObject>>();

	public void CreatePool(GameObject prefab, int poolSize)
	{
		int poolKey = prefab.GetInstanceID();
		if (!_poolDictionary.ContainsKey(poolKey))
		{
			_poolDictionary.Add(poolKey, new List<PoolObject>());

			GameObject poolHolder = CreateHolder(prefab);

			for (int i = 0; i < poolSize; i++)
			{
				AddToPool(prefab, poolKey, poolHolder.transform);
			}
		}
	}

	public GameObject Get(GameObject prefab)
	{
		int poolKey = prefab.GetInstanceID ();
		if (_poolDictionary.ContainsKey (poolKey) && _poolDictionary[poolKey].Count > 0) 
		{
			foreach (PoolObject obj in _poolDictionary[poolKey])
			{
				if(obj.GetComponent<PoolObject>().isFree)
				{
					obj.isFree = false;
					return obj.gameObject;
				}
			}
			if(_poolDictionary[poolKey].Count > 0)
			{
				Transform poolHolder = _poolDictionary[poolKey][0].GetComponent<PoolObject>().Holder;
				GameObject reuseObject = AddToPool(prefab, poolKey, poolHolder);
				
				reuseObject.GetComponent<PoolObject>().isFree = false;
				return reuseObject;
			}
		}
		else
		{
			_poolDictionary.Add(poolKey, new List<PoolObject>());

			GameObject poolHolder = CreateHolder(prefab);
			GameObject reuseObject = AddToPool(prefab, poolKey, poolHolder.transform);
			reuseObject.GetComponent<PoolObject>().isFree = false;
			return reuseObject;
		}
		
		return null;
	}

	public void Back(GameObject obj)
	{
		PoolObject poolObject = obj.GetComponent<PoolObject>();
		obj.transform.SetParent(poolObject.Holder);
		poolObject.isFree = true;
	}

	private GameObject AddToPool(GameObject prefab, int poolKey, Transform poolHolder)
	{
		GameObject newObject = Instantiate(prefab) as GameObject;
		PoolObject poolObject = newObject.GetComponent<PoolObject>();
		_poolDictionary[poolKey].Add(poolObject);
		poolObject.Holder = poolHolder;
		newObject.transform.SetParent (poolHolder);
		poolObject.isFree = true;
		return newObject;
	}

	private GameObject CreateHolder(GameObject prefab)
	{
		GameObject poolHolder = new GameObject (prefab.name + " pool");
		poolHolder.transform.SetParent(transform);
		return poolHolder;
	}
	
}
