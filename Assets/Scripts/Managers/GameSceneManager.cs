﻿using UnityEngine;
using UnityEngine.UI;
using TMPro;

public enum GameScene
{
	Game
	, Pause
	, Fail
}

public class GameSceneManager : Singleton<GameSceneManager> 
{
	public GameObject FailCanvas;
	public GameObject PauseCanvas;
	public GameObject PauseBtn;
	public GameObject LeftControls;
	public GameObject RightControls;
	public TextMeshProUGUI ScoreText;

	public TextMeshProUGUI ScoreFailText;

	private int _currentScore = 0;

	private void Start() 
	{	
		GetComponent<Canvas>().worldCamera = Camera.main;
		SetState(GameScene.Game);
		ScoreText.SetText("Score: 0");

		float size = Screen.width / 2.0f;
		LeftControls.GetComponent<RectTransform>().sizeDelta = new Vector2(size, size);
		RightControls.GetComponent<RectTransform>().sizeDelta = new Vector2(size, size);
	}

	public void SetState(GameScene newState)
	{
		switch (newState)
		{
			case GameScene.Game:
				Time.timeScale = 1.0f;
				FailCanvas.SetActive(false);
				PauseCanvas.SetActive(false);
				PauseBtn.SetActive(true);
			break;
			case GameScene.Fail:
				Time.timeScale = 0.0f;
				FailCanvas.SetActive(true);
				PauseCanvas.SetActive(false);
				PauseBtn.SetActive(false);
				ScoreFailText.SetText(GameManager.Instance.UpdateMaxScore(_currentScore));
			break;
			case GameScene.Pause:
				Time.timeScale = 0.0f;
				FailCanvas.SetActive(false);
				PauseCanvas.SetActive(true);
				PauseBtn.SetActive(false);
			break;
			default:
			break;
		}
	}

	public void IncreaseScore()
	{
		_currentScore++;
		ScoreText.SetText(" Score: " + _currentScore.ToString());
	}
	
	public void Pause()
	{
		SetState(GameScene.Pause);
	}

	public void Resume()
	{
		SetState(GameScene.Game);
	}

	public void Restart()
	{
		GameManager.Instance.StartGame();
	}

	public void Menu()
	{
		GameManager.Instance.Menu();
	}
}
