﻿using System.Collections.Generic;
using UnityEngine;

public class LineManager : Singleton<LineManager> 
{
	public float MinY = -6.0f;
	
	private List<Line> _lines;
	private bool _hasCurrentLine;
	private Line _currentLine;
	private LineGenerator _lineGenerator;
	private LineSpawner _lineSpawner;

	public override void Awake() 
	{
		base.Awake();
		_lineGenerator = GetComponent<LineGenerator>();
		_lineSpawner = GetComponent<LineSpawner>();
		_lines = new List<Line>();
	}

	public void TypeDirection(ArrowDirection direction)
	{
		if (_hasCurrentLine) 
		{
			if (_currentLine.GetNextDirection () == direction) 
			{
				_currentLine.TypeDirection ();
			} 
		} 
		else 
		{
			foreach (var arrow in _lines)
			{
				if(arrow.GetNextDirection() == direction)
				{
					_currentLine = arrow;
					_hasCurrentLine = true;
					_currentLine.SetCurrent(_hasCurrentLine);
					arrow.TypeDirection ();
					break;
				}	
			}
		}

		if (_hasCurrentLine && _currentLine.Typed()) 
		{
			_hasCurrentLine = false;
			_lines.Remove (_currentLine);
			GameSceneManager.Instance.IncreaseScore();
			_currentLine.SetCurrent(_hasCurrentLine);
			_currentLine = null;
		}
	}

	public void TypeAll()
	{
		while(_lines.Count > 0)
		{
			_lines[_lines.Count-1].TypeLine();
			_hasCurrentLine = false;
			_lines.RemoveAt(_lines.Count-1);
			GameSceneManager.Instance.IncreaseScore();
			_currentLine.SetCurrent(_hasCurrentLine);
			_currentLine = null;
		}
	}

	public void AddLine(float speed = 1.0f)
	{
		Line line = new Line (_lineGenerator.GetRandomArrowLine(), _lineSpawner.SpawnLine());
		line.SetLocation(Random.Range(-1, 1), 5.5f);
		line.SetSpeed(speed);
		_lines.Add (line);
	}
}
