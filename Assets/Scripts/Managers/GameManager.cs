﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : Singleton<GameManager> 
{
	public GameSettings Settings;

	private int _maxScore = 0;

	public override void Awake() 
	{
		base.Awake();
	}

	private void Start() 
	{
		Menu();
	}

	public void StartGame()
	{
		SceneManager.LoadScene("Game");
	}

	public void Menu()
	{
		SceneManager.LoadScene("Menu");
	}

	public string UpdateMaxScore(int currentScore)
	{
		string s = "Score: " + currentScore;
		if(_maxScore < currentScore)
		{
			_maxScore = currentScore;
			s += "\nNEW RECORD!";
		}
		else
		{
			s += "\nMax score: " + _maxScore;
		}
		return s;
	}

	public int MaxScore
	{
		get { return _maxScore; }
	}

}
