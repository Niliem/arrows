﻿using UnityEngine;
using UnityEngine.Audio;

namespace Audio
{
    public class AudioManager : Singleton<AudioManager> 
    {
        public AudioMixer mixer;
        public AudioGroup[] groups;

        public Sound[] sounds;

        public override void Awake()
        {
            base.Awake();

            foreach (Sound sound in sounds)
            {
                sound.source = gameObject.AddComponent<AudioSource>();
                sound.source.clip = sound.audioClip;
                sound.source.volume = sound.volume;
                sound.source.outputAudioMixerGroup = sound.mixerGroup;
                sound.source.loop = sound.loop;
            }            
        }

        private void Start() 
        {
            foreach (AudioGroup group in groups)
            {
                SetParamValue(group.paramName, group.volume);
                if(group.muted)
                {
                    SetParamValue(group.paramName, -80.0f);
                }
            }

            Play("MainMusic");
        }

        public void Play(string name)
        {
            Sound s = System.Array.Find(sounds, sound => sound.name == name);
            if(s == null)
            {
                Debug.LogWarning("Sound: " + name + " not found!");
                return;
            }
            s.source.Play();
        }

        public void MuteAudioGroup(string name, bool mute)
        {
            AudioGroup g = System.Array.Find(groups, group => group.name == name);
            if(g == null)
            {
                Debug.LogWarning("Audio group: " + name + " not found!");
                return;
            }
            g.muted = mute;
            SetParamValue(g.paramName, (g.muted) ? -80.0f : g.volume);
        }

        public bool isMuteAudioGroup(string name)
        {
            AudioGroup g = System.Array.Find(groups, group => group.name == name);
            if(g == null)
            {
                Debug.LogWarning("Audio group: " + name + " not found!");
                return true;
            }
            return g.muted;
        }

        public void SetAudioGroupVolume(string name, float volume)
        {
            AudioGroup g = System.Array.Find(groups, group => group.name == name);
            if(g == null)
            {
                Debug.LogWarning("Audio group: " + name + " not found!");
                return;
            }
            g.muted = !(volume > -80.0f);
            SetParamValue(g.paramName, g.volume);
        }

        private void SetParamValue(string name, float value)
        {
            mixer.SetFloat(name, value);
        }

    }
}
